/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2016 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "Segmentor.h"

// CamiTK includes
#include <ActionWidget.h>
#include <Application.h>
#include <InteractiveViewer.h>
#include <Property.h>

// Qt includes
#include <QBoxLayout>
#include <QGroupBox>
#include <QPushButton>
#include <QRadioButton>
#include <QString>

// VTK includes
#include <vtkBoundedPlanePointPlacer.h>
#include <vtkCleanPolyData.h>
#include <vtkContourTriangulator.h>
#include <vtkImageDataGeometryFilter.h>
#include <vtkImageStencil.h>
#include <vtkLinearExtrusionFilter.h>
#include <vtkMarchingCubes.h>
#include <vtkOrientedGlyphContourRepresentation.h>
#include <vtkPolyDataToImageStencil.h>
#include <vtkProperty.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkThresholdPoints.h>

using namespace camitk;

// --------------- Constructor -------------------
Segmentor::Segmentor(ActionExtension * extension) : Action(extension) {
    // Setting name, description and input component
    setName("Segmentor");
    setDescription("This action lets the user manually segment images using ITK-Snap like tool.");
    setComponent("ImageComponent");

    // Setting classification family and tags
    setFamily("Tutorial");
    addTag("Wiki");
    addTag("Segmentation");

    // Setting the action's parameters
    addParameter(new Property(tr("Contour Color"), QColor(255, 0, 255, 192), tr("Color of the contour"), ""));
    addParameter(new Property(tr("Contour Width"), 1.5, tr("Line width of the contour"), ""));
    
    informationFrame = NULL;
    dynamic_cast<ActionWidget*>(Action::getWidget())->setButtonVisibility( false );
    
    currentViewer = Axial;
    contourWidget = NULL;
    currentImageComp = NULL;
    segmentedImage = NULL;
    segmentedMesh = NULL;    
}

// --------------- destructor -------------------
Segmentor::~Segmentor() {
    // Do not do anything yet.
    // Delete stuff if you create stuff
    // (except if you use smart pointers of course !!)
}

// ---------------- getWidget --------------------
QWidget * Segmentor::getWidget() {
    // Update image
    ImageComponent * selectedImageCmp = dynamic_cast<ImageComponent*>(getTargets().last());
    
    // Check if the current image is still the same
    if (selectedImageCmp != currentImageComp) {
        // Update image
        currentImageComp = selectedImageCmp;        
        
        // Initialize the contour
        resetCurrentContourWidget();
        
        // Delete the segmented image if it already exist
        if (segmentedImage) {
            segmentedImage = NULL;
        }
        
        // Delete the segmented mesh if it already exist
        if (segmentedMesh) {
            segmentedMesh->setModified( false );
            Application::close( segmentedMesh );
            Application::refresh();
            segmentedMesh = NULL;
        }
        
        // Create the segmented image
        if (! segmentedImage) {
            double * origin = currentImageComp->getImageData()->GetOrigin( );
            double * spacing = currentImageComp->getImageData()->GetSpacing( );
            int * dims = currentImageComp->getImageData()->GetDimensions( );
            
            segmentedImage = vtkSmartPointer<vtkImageData>::New();
            segmentedImage->SetDimensions( dims );
            segmentedImage->AllocateScalars(VTK_UNSIGNED_CHAR, 1);
            segmentedImage->SetOrigin( origin );
            segmentedImage->SetSpacing( spacing );
            
            vtkIdType count = segmentedImage->GetNumberOfPoints();
            unsigned char * ptr = static_cast<unsigned char *>(segmentedImage->GetScalarPointer(0, 0, 0));
            for (vtkIdType index = 0; index < count; index++){
                *ptr++ = 0;
            }
        }
    }

    // Create widget
    if ( ! informationFrame) {
        informationFrame = new QFrame();
        informationFrame->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
        informationFrame->setLineWidth(3);

        // Put all GUI elements in a vertical layout
        QVBoxLayout * informationFrameLayout = new QVBoxLayout();

        // Add the default action widget
        informationFrameLayout->addWidget(Action::getWidget());
        
        // Available viewers
        QRadioButton * axialBtn = new QRadioButton("Axial", NULL);
        axialBtn->setObjectName("axialBtn");
        QObject::connect(axialBtn, SIGNAL(clicked()), this, SLOT(setCurrentViewer()));
        QObject::connect(axialBtn, SIGNAL(clicked()), this, SLOT(resetCurrentContourWidget()));
        
        QRadioButton * coronalBtn = new QRadioButton("Coronal", NULL);
        coronalBtn->setObjectName("coronalBtn");
        QObject::connect(coronalBtn, SIGNAL(clicked()), this, SLOT(setCurrentViewer()));
        QObject::connect(coronalBtn, SIGNAL(clicked()), this, SLOT(resetCurrentContourWidget()));
        
        QRadioButton * sagittalBtn = new QRadioButton("Sagittal", NULL);
        sagittalBtn->setObjectName("sagittalBtn");
        QObject::connect(sagittalBtn, SIGNAL(clicked()), this, SLOT(setCurrentViewer()));
        QObject::connect(sagittalBtn, SIGNAL(clicked()), this, SLOT(resetCurrentContourWidget()));
        
        // Group them together
        QGroupBox * viewGroup = new QGroupBox("Viewer");
        QHBoxLayout * viewHBox = new QHBoxLayout();
        viewHBox->addWidget( axialBtn );
        viewHBox->addWidget( coronalBtn );
        viewHBox->addWidget( sagittalBtn );
        viewGroup->setLayout( viewHBox );
        informationFrameLayout->addWidget(viewGroup);
        
        // Controls for one slice
        QPushButton * sliceCompleteBtn = new QPushButton("Complete");
        QObject::connect(sliceCompleteBtn, SIGNAL(clicked()), this, SLOT(completeCurrentContour()));
        
        QPushButton * sliceResetBtn = new QPushButton("Reset");
        QObject::connect(sliceResetBtn, SIGNAL(clicked()), this, SLOT(resetCurrentContourWidget()));
        
        QPushButton * sliceAcceptBtn = new QPushButton("Accept");
        sliceAcceptBtn->setObjectName("sliceAcceptBtn");
        sliceAcceptBtn->setEnabled(false);
        QObject::connect(sliceAcceptBtn, SIGNAL(clicked()), this, SLOT(acceptCurrentContour()));
        
        QPushButton * slicePasteBtn = new QPushButton("Paste Last");
        slicePasteBtn->setObjectName("slicePasteBtn");
        slicePasteBtn->setEnabled(false);
        QObject::connect(slicePasteBtn, SIGNAL(clicked()), this, SLOT(pasteLastContour()));

        // Group them together
        QGroupBox * sliceGroup = new QGroupBox("Slice Only");        
        QVBoxLayout * sliceVBox = new QVBoxLayout();
        sliceVBox->addWidget(sliceCompleteBtn);
        sliceVBox->addWidget(sliceResetBtn);
        sliceVBox->addWidget(sliceAcceptBtn);
        sliceVBox->addWidget(slicePasteBtn);
        sliceGroup->setLayout(sliceVBox);        
        informationFrameLayout->addWidget(sliceGroup);
        
        // Controls for the whole image
        QPushButton * allResetBtn = new QPushButton("Reset");
        QObject::connect(allResetBtn, SIGNAL(clicked()), this, SLOT(resetEverything()));
        
        QPushButton * saveImageBtn = new QPushButton("Save As Image");
        QObject::connect(saveImageBtn, SIGNAL(clicked()), this, SLOT(saveSegmentedImage()));
        
        // Group them together
        QGroupBox * allGroup = new QGroupBox("Whole Image");
        QVBoxLayout * allVBox = new QVBoxLayout();
        allVBox->addWidget(allResetBtn);
        allVBox->addWidget(saveImageBtn);
        allGroup->setLayout(allVBox);
        informationFrameLayout->addWidget(allGroup);
        
        // Set the layout for the action widget
        informationFrame->setLayout(informationFrameLayout);
        
        // Set the axial viewer by default
        informationFrame->findChild<QRadioButton *>("axialBtn")->click();
        
        // Enable automatic updating of the properties
        dynamic_cast<ActionWidget*>(Action::getWidget())->setAutoUpdateProperty(true);
        
        // Create mappers
        axialMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        coronalMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        sagittalMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        
        // Turn off scalar visibility to enable coloring using the contour colors
        axialMapper->SetScalarVisibility(0);
        coronalMapper->SetScalarVisibility(0);
        sagittalMapper->SetScalarVisibility(0);

        // Create actors
        axialActor = vtkSmartPointer<vtkActor>::New();
        coronalActor = vtkSmartPointer<vtkActor>::New();
        sagittalActor = vtkSmartPointer<vtkActor>::New();
        
        // Set the corresponding mappers
        axialActor->SetMapper(axialMapper);
        coronalActor->SetMapper(coronalMapper);
        sagittalActor->SetMapper(sagittalMapper);
        
        // Turn off the lighting to make the actor color more visible
        axialActor->GetProperty()->LightingOff();
        coronalActor->GetProperty()->LightingOff();
        sagittalActor->GetProperty()->LightingOff();
        
        // Add actors to the corresponding views
        InteractiveViewer::getAxialViewer()->getRendererWidget()->addProp( axialActor, true );
        InteractiveViewer::getCoronalViewer()->getRendererWidget()->addProp( coronalActor, true );
        InteractiveViewer::getSagittalViewer()->getRendererWidget()->addProp( sagittalActor, true );
        
        // Handle slider changed events
        QObject::connect(InteractiveViewer::getAxialViewer(), SIGNAL(selectionChanged()), this, SLOT(updateOverlayOnSingle2DViewer()));
        QObject::connect(InteractiveViewer::getCoronalViewer(), SIGNAL(selectionChanged()), this, SLOT(updateOverlayOnSingle2DViewer()));
        QObject::connect(InteractiveViewer::getSagittalViewer(), SIGNAL(selectionChanged()), this, SLOT(updateOverlayOnSingle2DViewer()));
    }
    
    return informationFrame;
}

// --------------- apply -------------------
Action::ApplyStatus Segmentor::apply() {
    return SUCCESS;
}

// --------------- setCurrentViewer -------------------
void Segmentor::setCurrentViewer() {
    QRadioButton * button = informationFrame->findChild<QRadioButton *>("sagittalBtn");
    if (button->isChecked()) {
        currentViewer = Sagittal;
        return;
    }
    button = informationFrame->findChild<QRadioButton *>("coronalBtn");
    if (button->isChecked()) {
        currentViewer = Coronal;
        return;
    }
    button = informationFrame->findChild<QRadioButton *>("axialBtn");
    if (button->isChecked()) {
        currentViewer = Axial;
        return;
    }
}

// --------------- resetCurrentContourWidget -------------------
void Segmentor::resetCurrentContourWidget() {
    resetCurrentContourWidget( false, NULL );

    // Disable Accept and Paste last buttons
    changeButtonAvailability("sliceAcceptBtn", false);
    changeButtonAvailability("slicePasteBtn", false);
}

// --------------- completeCurrentContour -------------------
void Segmentor::completeCurrentContour() {
    
    if (contourWidget->GetWidgetState() == vtkContourWidget::Define) {
        vtkSmartPointer<vtkOrientedGlyphContourRepresentation> contourRep = vtkOrientedGlyphContourRepresentation::SafeDownCast(contourWidget->GetRepresentation());
        if (contourRep->GetNumberOfNodes() > 2) {
            contourWidget->CloseLoop();
            
            // Enable Accept and disable Paste last buttons
            changeButtonAvailability("sliceAcceptBtn", true);
            changeButtonAvailability("slicePasteBtn", false);

            return;
        }
    }
    Application::showStatusBarMessage("At least two nodes are needed to close the contour");
}

// --------------- acceptCurrentContour -------------------
void Segmentor::acceptCurrentContour() {
    updateSegmentedImage();
    drawOverlaysOn2DViewers(true);
    updateSegmentedMesh();
    contourWidget->Off();

    // Disable Accept and enable Paste last buttons
    changeButtonAvailability("sliceAcceptBtn", false);
    changeButtonAvailability("slicePasteBtn", true);   
}

// --------------- updateOverlayOnSingle2DViewer -------------------
void Segmentor::updateOverlayOnSingle2DViewer() {
    QString whichViewer = this->sender()->objectName();
    if (! whichViewer.compare("sagittalViewer")) {
        drawOverlaysOn2DViewers(false, Sagittal);
    }
    else if (! whichViewer.compare("coronalViewer")) {
        drawOverlaysOn2DViewers(false, Coronal);
    }
    else {
        drawOverlaysOn2DViewers(false, Axial);
    }
}

// --------------- updateOverlayOnSingle2DViewer -------------------
void Segmentor::pasteLastContour() {
    double viewCenter, oldViewCenter;
    vtkSmartPointer<vtkPolyData> polyData = contourWidget->GetContourRepresentation()->GetContourRepresentationAsPolyData();

    double translation[3] = {0.0,0.0,0.0};
    vtkSmartPointer<vtkTransform> displayTranformIn2D = vtkSmartPointer<vtkTransform>::New();
    
    double * spacing = currentImageComp->getImageData()->GetSpacing();
    double polyTolerance;
    
    if (currentViewer == Sagittal) {
        viewCenter = currentImageComp->getSagittalSlices()->getSlice() * currentImageComp->getImageData()->GetSpacing()[0];
        oldViewCenter = polyData->GetPoints()->GetPoint(0)[0];
        translation[0] = viewCenter - oldViewCenter;
        polyTolerance = (spacing[1] + spacing[2])/20;
    }
    else if (currentViewer == Coronal) {
        viewCenter = currentImageComp->getCoronalSlices()->getSlice() * currentImageComp->getImageData()->GetSpacing()[1];
        oldViewCenter = polyData->GetPoints()->GetPoint(0)[1];
        translation[1] = viewCenter - oldViewCenter;
        polyTolerance = (spacing[2] + spacing[0])/20;
    }
    else{
        viewCenter = currentImageComp->getAxialSlices()->getSlice() * currentImageComp->getImageData()->GetSpacing()[2];
        oldViewCenter = polyData->GetPoints()->GetPoint(0)[2];
        translation[2] = viewCenter - oldViewCenter;
        polyTolerance = (spacing[0] + spacing[1])/20;
    }
    displayTranformIn2D->Translate( translation );

    vtkSmartPointer<vtkTransformPolyDataFilter> transformPolyDataFilter = vtkTransformPolyDataFilter::New();
    transformPolyDataFilter->SetTransform( displayTranformIn2D );
    transformPolyDataFilter->SetInputData( polyData );
    
    // Clean the near duplicates and duplicates
    vtkSmartPointer<vtkCleanPolyData> cleanFilter = vtkSmartPointer<vtkCleanPolyData>::New();
    cleanFilter->SetTolerance( polyTolerance );
    cleanFilter->SetInputConnection( transformPolyDataFilter->GetOutputPort() );
    cleanFilter->Update();
    
    resetCurrentContourWidget(true, cleanFilter->GetOutput());

    // Enable Accept and disable Paste last buttons
    changeButtonAvailability("sliceAcceptBtn", true);
    changeButtonAvailability("slicePasteBtn", false);
}

// --------------- resetEverything -------------------
void Segmentor::resetEverything() {
    // Clear the segmented image
    int * dims = segmentedImage->GetDimensions( );
    vtkIdType count = segmentedImage->GetNumberOfPoints();
    unsigned char * ptr = static_cast<unsigned char *>(segmentedImage->GetScalarPointer(0, 0, 0));
    for (vtkIdType index = 0; index < count; index++){
        *ptr++ = 0;
    }    
    // Clear the segmented mesh
    updateSegmentedMesh();
    // Clear the 2D viewers
    drawOverlaysOn2DViewers(true);
    
    // Disable Accept and Paste last buttons
    changeButtonAvailability("sliceAcceptBtn", false);
    changeButtonAvailability("slicePasteBtn", false);
    
    Application::showStatusBarMessage("Segmented image, mesh and regions were successfully reset");
}

// --------------- saveSegmentedImage -------------------
void Segmentor::saveSegmentedImage() {
    ImageComponent * tempImg = new ImageComponent( segmentedImage, currentImageComp->getName() + "_Seg.mhd" );
    tempImg->setSelected( true );
    Application::save( tempImg );
    tempImg->setModified( false );
    Application::close( tempImg );
    Application::refresh();
    
    Application::showStatusBarMessage("Segmented image successfully saved");
}

// --------------- updateSegmentedImage -------------------
void Segmentor::updateSegmentedImage() {
    // Get the region inside the contour triangulated
    vtkSmartPointer<vtkContourTriangulator> triangulateFilter = vtkSmartPointer<vtkContourTriangulator>::New();
    triangulateFilter->SetInputData( contourWidget->GetContourRepresentation()->GetContourRepresentationAsPolyData() );
    triangulateFilter->Update();
    
    // Sweep polygonal data
    vtkSmartPointer<vtkLinearExtrusionFilter> extruder = vtkSmartPointer<vtkLinearExtrusionFilter>::New();
    extruder->SetScaleFactor(1.0);
    extruder->SetExtrusionTypeToNormalExtrusion();
    if (currentViewer == Axial) {
        extruder->SetInputData(triangulateFilter->GetOutput());
    }
    else {
        vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
        vtkSmartPointer<vtkTransform> transform2 = vtkSmartPointer<vtkTransform>::New();
        
        if (currentViewer == Sagittal) {
            transform->RotateX(-90);
            transform2->RotateY(-90);
        }
        else if (currentViewer == Coronal){
            transform->RotateZ(90);
            transform2->RotateY(90);
            
        }
    
        vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
        vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter2 = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
    
        transformFilter->SetTransform(transform);
        transformFilter->SetInputData(triangulateFilter->GetOutput());
        
        transformFilter2->SetTransform(transform2);
        transformFilter2->SetInputConnection( transformFilter->GetOutputPort() );
        
        transformFilter2->Update();
        extruder->SetInputData(transformFilter2->GetOutput());
    }
    extruder->Update();
    
    double * spacing, * origin, newSpacing[3], newOrigin[3];
    int * dims, * extent, newDims[3], newExtent[6];
    
    origin = currentImageComp->getImageData()->GetOrigin( );
    spacing = currentImageComp->getImageData()->GetSpacing( );
    dims = currentImageComp->getImageData()->GetDimensions( );
    extent = currentImageComp->getImageData()->GetExtent( );

    if (currentViewer == Sagittal) {
        //x :: y :: z => y :: z :: x
        newOrigin[0] = origin[1];   newOrigin[1] = origin[2];   newOrigin[2] = origin[0];
        newSpacing[0] = spacing[1]; newSpacing[1] = spacing[2]; newSpacing[2] = spacing[0];
        newDims[0] = dims[1];       newDims[1] = dims[2];       newDims[2] = dims[0];
        newExtent[0] = extent[2];   newExtent[1] = extent[3];   newExtent[2] = extent[4];
        newExtent[3] = extent[5];   newExtent[4] = extent[0];   newExtent[5] = extent[1];
    }
    else if (currentViewer == Coronal) {
        //x :: y :: z => z :: x :: y
        newOrigin[0] = origin[2];   newOrigin[1] = origin[0];   newOrigin[2] = origin[1];
        newSpacing[0] = spacing[2]; newSpacing[1] = spacing[0]; newSpacing[2] = spacing[1];
        newDims[0] = dims[2];       newDims[1] = dims[0];       newDims[2] = dims[1];
        newExtent[0] = extent[4];   newExtent[1] = extent[5];   newExtent[2] = extent[0];
        newExtent[3] = extent[1];   newExtent[4] = extent[2];   newExtent[5] = extent[3];
    }
    else {
        //x :: y :: z => x :: y :: z
        newOrigin[0] = origin[0];   newOrigin[1] = origin[1];   newOrigin[2] = origin[2];
        newSpacing[0] = spacing[0]; newSpacing[1] = spacing[1]; newSpacing[2] = spacing[2];
        newDims[0] = dims[0];       newDims[1] = dims[1];       newDims[2] = dims[2];
        newExtent[0] = extent[0];   newExtent[1] = extent[1];   newExtent[2] = extent[2];
        newExtent[3] = extent[3];   newExtent[4] = extent[4];   newExtent[5] = extent[5];
    }
    
    // Create an image stencil from the poly data
    vtkSmartPointer<vtkPolyDataToImageStencil> pol2stenc = vtkSmartPointer<vtkPolyDataToImageStencil>::New();
    pol2stenc->SetTolerance(0);
    pol2stenc->SetInputConnection( extruder->GetOutputPort() );
    pol2stenc->SetOutputOrigin( newOrigin );
    pol2stenc->SetOutputSpacing( newSpacing );
    pol2stenc->SetOutputWholeExtent( newExtent );
    pol2stenc->Update();

    
    // Create another temporary image to find the instersection
    vtkSmartPointer<vtkImageData> whiteImage = vtkSmartPointer<vtkImageData>::New();
    whiteImage->SetSpacing( newSpacing );
    whiteImage->SetDimensions( newDims );
    whiteImage->SetOrigin( newOrigin );
    whiteImage->AllocateScalars(VTK_UNSIGNED_CHAR,1);
    vtkIdType count = whiteImage->GetNumberOfPoints();
    unsigned char * ptr = static_cast<unsigned char *>(whiteImage->GetScalarPointer(0, 0, 0));
    for (vtkIdType index = 0; index < count; index++){
        *ptr++ = 255;
    }
    
    // Cut the corresponding white image and set the background
    vtkSmartPointer<vtkImageStencil> imgstenc = vtkSmartPointer<vtkImageStencil>::New();
    imgstenc->SetInputData( whiteImage );
    imgstenc->SetStencilConnection(pol2stenc->GetOutputPort());
    imgstenc->ReverseStencilOff();
    imgstenc->SetBackgroundValue( 0 );
    imgstenc->Update();
    
    // Update the segmentedImage using the intersected region
    vtkSmartPointer<vtkImageData> stencil = imgstenc->GetOutput();
    if (currentViewer == Sagittal) {
        double xCenter = currentImageComp->getSagittalSlices()->getSlice( );
        for(int j = 0; j < newDims[1]; j++){
            for(int i = 0; i < newDims[0]; i++){
                unsigned char * ptrStencil = static_cast<unsigned char *>(stencil->GetScalarPointer( i, j, xCenter));
                if( * ptrStencil == 255 ){
                    unsigned char * ptr = static_cast<unsigned char *>(segmentedImage->GetScalarPointer( xCenter, i, j));
                    *ptr = 255;
                }
            }
        }
    }
    else if (currentViewer == Coronal) {
        double yCenter = currentImageComp->getCoronalSlices()->getSlice( );
        for(int j = 0; j < newDims[1]; j++){
            for(int i = 0; i < newDims[0]; i++){
                unsigned char * ptrStencil = static_cast<unsigned char *>(stencil->GetScalarPointer( i, j, yCenter));
                if( * ptrStencil == 255 ){
                    unsigned char * ptr = static_cast<unsigned char *>(segmentedImage->GetScalarPointer( j, yCenter, i));
                    *ptr = 255;
                }
            }
        }
    }
    else {
        double zCenter = currentImageComp->getAxialSlices()->getSlice( );
        for(int j = 0; j < newDims[1]; j++){
            for(int i = 0; i < newDims[0]; i++){
                unsigned char * ptrStencil = static_cast<unsigned char *>(stencil->GetScalarPointer( i, j, zCenter));
                if( * ptrStencil == 255 ){
                    unsigned char * ptr = static_cast<unsigned char *>(segmentedImage->GetScalarPointer( i, j, zCenter));
                    *ptr = 255;
                }
            }
        }
    }
    
}

// --------------- drawOverlaysOn2DViewers -------------------
void Segmentor::drawOverlaysOn2DViewers(const bool drawOnAll2DViewers, Viewer whichViewer) {
    
    // Choose on which viewer to draw
    bool axialOn = false, coronalOn = false, sagittalOn = false;
    if (drawOnAll2DViewers) {
        axialOn = true;
        coronalOn = true;
        sagittalOn = true;
    }
    else if (whichViewer == Sagittal) {
        sagittalOn = true;
    }
    else if (whichViewer == Coronal) {
        coronalOn = true;
    }
    else {
        axialOn = true;
    }
    
    int * extent = currentImageComp->getImageData()->GetExtent();    
    QColor contourColor = property("Contour Color").value<QColor>();

    if (sagittalOn) {
        int currentSagittal = currentImageComp->getSagittalSlices()->getSlice();
        sagittalActor->GetProperty()->SetColor( contourColor.redF(), contourColor.greenF(), contourColor.blueF() );

        vtkSmartPointer<vtkImageDataGeometryFilter> sagittalImageDataGeometryFilter = vtkSmartPointer<vtkImageDataGeometryFilter>::New();
        sagittalImageDataGeometryFilter->SetInputData( segmentedImage );
        sagittalImageDataGeometryFilter->SetExtent( currentSagittal, currentSagittal, extent[2], extent[3], extent[4], extent[5] );
        sagittalImageDataGeometryFilter->Update();
        
        vtkSmartPointer<vtkThresholdPoints> sagittalThresholdFilter =vtkSmartPointer<vtkThresholdPoints>::New();
        sagittalThresholdFilter->SetInputConnection( sagittalImageDataGeometryFilter->GetOutputPort() );
        sagittalThresholdFilter->ThresholdByUpper( 255 );
        sagittalThresholdFilter->Update();
        
        sagittalMapper->RemoveAllInputs();
        sagittalMapper->SetInputConnection( sagittalThresholdFilter->GetOutputPort() );
        InteractiveViewer::getSagittalViewer()->refresh();
    }

    if (coronalOn) {
        int currentCoronal = currentImageComp->getCoronalSlices()->getSlice();
        coronalActor->GetProperty()->SetColor( contourColor.redF(), contourColor.greenF(), contourColor.blueF() );
        
        vtkSmartPointer<vtkImageDataGeometryFilter> coronalImageDataGeometryFilter = vtkSmartPointer<vtkImageDataGeometryFilter>::New();
        coronalImageDataGeometryFilter->SetInputData( segmentedImage );
        coronalImageDataGeometryFilter->SetExtent( extent[0], extent[1], currentCoronal, currentCoronal, extent[4], extent[5] );
        coronalImageDataGeometryFilter->Update();
        
        vtkSmartPointer<vtkThresholdPoints> coronalThresholdFilter =vtkSmartPointer<vtkThresholdPoints>::New();
        coronalThresholdFilter->SetInputConnection( coronalImageDataGeometryFilter->GetOutputPort() );
        coronalThresholdFilter->ThresholdByUpper( 255 );
        coronalThresholdFilter->Update();
        
        coronalMapper->RemoveAllInputs();
        coronalMapper->SetInputConnection( coronalThresholdFilter->GetOutputPort() );
        InteractiveViewer::getCoronalViewer()->refresh();
    }
    
    if (axialOn) {
        int currentAxial = currentImageComp->getAxialSlices()->getSlice();
        axialActor->GetProperty()->SetColor( contourColor.redF(), contourColor.greenF(), contourColor.blueF() );
        
        vtkSmartPointer<vtkImageDataGeometryFilter> axialImageDataGeometryFilter = vtkSmartPointer<vtkImageDataGeometryFilter>::New();
        axialImageDataGeometryFilter->SetInputData( segmentedImage );
        axialImageDataGeometryFilter->SetExtent( extent[0], extent[1], extent[2], extent[3], currentAxial, currentAxial );
        axialImageDataGeometryFilter->Update();

        vtkSmartPointer<vtkThresholdPoints> axialThresholdFilter =vtkSmartPointer<vtkThresholdPoints>::New();
        axialThresholdFilter->SetInputConnection( axialImageDataGeometryFilter->GetOutputPort() );
        axialThresholdFilter->ThresholdByUpper( 255 );
        axialThresholdFilter->Update();
    
        axialMapper->RemoveAllInputs();
        axialMapper->SetInputConnection( axialThresholdFilter->GetOutputPort() );
        InteractiveViewer::getAxialViewer()->refresh();
    }
}

// --------------- updateSegmentedMesh -------------------
void Segmentor::updateSegmentedMesh() {
    double isoValue = 255;
    vtkSmartPointer<vtkMarchingCubes> surface = vtkSmartPointer<vtkMarchingCubes>::New();
    surface->SetInputData( segmentedImage );
    surface->ComputeNormalsOn();
    surface->SetValue(0, isoValue);
    surface->Update();
    
    // Everything happens in current image frame but the 3D viewer is set in the world frame
    // hence we need to transform everything back to the world coordinates
    vtkSmartPointer<vtkTransformPolyDataFilter> transformFromImageToWorld;
    transformFromImageToWorld = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
    transformFromImageToWorld->SetTransform(currentImageComp->getTransformFromWorld());
    transformFromImageToWorld->SetInputData(surface->GetOutput());
    transformFromImageToWorld->Update();  

    if (! segmentedMesh) {
        segmentedMesh = new MeshComponent( transformFromImageToWorld->GetOutput(), currentImageComp->getName() + "_Seg");
        segmentedMesh->getActor(InterfaceGeometry::Surface)->GetMapper()->SetScalarVisibility(0);
        Application::refresh();
    }
    else {
        segmentedMesh->setPointSet( transformFromImageToWorld->GetOutput() );
    }
    QColor contourColor = property("Contour Color").value<QColor>();
    segmentedMesh->setColor(contourColor.redF(), contourColor.greenF(), contourColor.blueF());
    segmentedMesh->setOpacity(contourColor.alphaF());
    InteractiveViewer::get3DViewer()->refresh();
}

// --------------- resetCurrentContourWidget -------------------
void Segmentor::resetCurrentContourWidget(const bool initialDataProvided, vtkPolyData * initialData ) {
    // Reset any observers before killing the widget
    if (contourWidget) {
        contourWidget->RemoveObserver(this);
        contourWidget->Off();
        contourWidget = NULL;
    }

    // Create the widget again
    contourWidget = vtkSmartPointer<vtkContourWidget>::New();

    // Find the focused slice and set the interactor depending on the viewer chosen
    vtkRenderWindowInteractor * interactor = NULL;
    double viewCenter;
    if (currentViewer == Sagittal) {
        interactor = InteractiveViewer::getSagittalViewer()->getRendererWidget()->GetRenderWindow()->GetInteractor();
        viewCenter = currentImageComp->getSagittalSlices()->getSlice() * currentImageComp->getImageData()->GetSpacing()[0];
    }
    else if (currentViewer == Coronal) {
        interactor = InteractiveViewer::getCoronalViewer()->getRendererWidget()->GetRenderWindow()->GetInteractor();
        viewCenter = currentImageComp->getCoronalSlices()->getSlice() * currentImageComp->getImageData()->GetSpacing()[1];
    }
    else {
        interactor = InteractiveViewer::getAxialViewer()->getRendererWidget()->GetRenderWindow()->GetInteractor();
        viewCenter = currentImageComp->getAxialSlices()->getSlice() * currentImageComp->getImageData()->GetSpacing()[2];
    }
    contourWidget->SetInteractor(interactor);
    contourWidget->ContinuousDrawOn();
    contourWidget->On();
    // Use initial data if provided or create 3D representation otherwise
    if (initialDataProvided) {
        contourWidget->Initialize(initialData);
    }
    else {
        contourWidget->CreateDefaultRepresentation();
    }
    contourWidget->AddObserver(vtkCommand::EndInteractionEvent, this);

    // Move the representation to the focused slice depending on the viewer chosen
    vtkSmartPointer<vtkOrientedGlyphContourRepresentation> contourRep = vtkOrientedGlyphContourRepresentation::SafeDownCast(contourWidget->GetRepresentation());
    vtkSmartPointer<vtkBoundedPlanePointPlacer> frontPlanePlacer = vtkSmartPointer<vtkBoundedPlanePointPlacer>::New();
    contourRep->SetPointPlacer(frontPlanePlacer);
        
    if (currentViewer == Sagittal) {
        frontPlanePlacer->SetProjectionNormalToXAxis();
    }
    else if (currentViewer == Coronal) {
        frontPlanePlacer->SetProjectionNormalToYAxis();
    }
    else {
        frontPlanePlacer->SetProjectionNormalToZAxis();
    }
    frontPlanePlacer->SetProjectionPosition(viewCenter);
    
    // Initialize the interactor
    interactor->ReInitialize();

    // Update color and line width
    QColor contourColor = property("Contour Color").value<QColor>();
    contourRep->GetLinesProperty()->SetColor(contourColor.redF(), contourColor.greenF(), contourColor.blueF() );
    contourRep->GetLinesProperty()->SetLineWidth(property("Contour Width").toFloat());
    
    Application::refresh();
}

// --------------- changeButtonAvailability -------------------
void Segmentor::changeButtonAvailability(const QString buttonName, const bool isAvailable) {
    if (informationFrame) {
        QPushButton * button = informationFrame->findChild<QPushButton *>(buttonName);
        if (button) {
            button->setEnabled( isAvailable );
        }
    }
}

// --------------- Execute -------------------
void Segmentor::Execute(vtkObject * caller, unsigned long eventId, void * callData) {
    if (eventId == vtkCommand::EndInteractionEvent) {
        // Enable accept button
        changeButtonAvailability("sliceAcceptBtn", true);
    }
}