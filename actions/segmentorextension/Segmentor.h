/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2016 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef SEGMENTOR_H
#define SEGMENTOR_H

#include <Action.h>

#include <ImageComponent.h>
#include <MeshComponent.h>

#include <QFrame>

#include <vtkCommand.h>
#include <vtkContourWidget.h>
#include <vtkPolyDataMapper.h>


class Segmentor : public camitk::Action, public vtkCommand {
    Q_OBJECT
    
    /// Different orthogonal viewers
    Q_ENUMS(Viewer)

public:
    /// Different orthogonal viewers
    enum Viewer {Axial, Coronal, Sagittal, All};

    /// Default Constructor 
    Segmentor(camitk::ActionExtension *);

    /// Default Destructor
    virtual ~Segmentor();
    
    /// Method called when the action is triggered (i.e. started)
    virtual QWidget * getWidget();

public slots:
    /** This method is automatically called when the action is triggered.
      * Call getTargets() method to get the list of components to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components, 
      * i.e., instances of Segmentor (or a subclass).
      */
    virtual ApplyStatus apply();

private:
    /// Custom GUI widgets will be added to this widget and be returned by the getWidget() method
    QFrame * informationFrame;
    
    /// Currently selected viewer
    Viewer currentViewer;
    
    /// Interactive contour
    vtkSmartPointer<vtkContourWidget> contourWidget;
    
    /// Currently selected image component
    camitk::ImageComponent * currentImageComp;
    
    /// Image representing the current status of the segmentation
    vtkSmartPointer<vtkImageData> segmentedImage;
    
    /// Mapper for axial viewer
    vtkSmartPointer<vtkPolyDataMapper> axialMapper;
    
    /// Mapper for coronal viewer
    vtkSmartPointer<vtkPolyDataMapper> coronalMapper;
    
    /// Mapper for sagittal viewer
    vtkSmartPointer<vtkPolyDataMapper> sagittalMapper;
    
    /// Actor for axial viewer
    vtkSmartPointer<vtkActor> axialActor;
    
    /// Actor for coronal viewer
    vtkSmartPointer<vtkActor> coronalActor;
    
    /// Actor for sagittal viewer
    vtkSmartPointer<vtkActor> sagittalActor;
    
    /// Mesh component corresponding to the segmented image
    camitk::MeshComponent * segmentedMesh;

    /// Updates the segmentedImage with the latest region inside the contour
    void updateSegmentedImage( );
    
    /// Draws overlays over the current slice of each 2D viewer
    void drawOverlaysOn2DViewers(const bool drawOnAll2DViewers, Viewer whichViewer = All);
    
    /// Updates the segmentedMesh withe the latest region inside the contour
    void updateSegmentedMesh( );
    
    /// Resets the current contour
    void resetCurrentContourWidget(const bool initialDataProvided, vtkPolyData * initialData = NULL );
    
    /// Enables or disables a given button
    void changeButtonAvailability(const QString buttonName, const bool isAvailable);

    /// Overrides the VTK callback inherited from vtkCommand
    void Execute(vtkObject * caller, unsigned long eventId, void * callData);

private slots:
    /// Sets the currently selected viewer
    void setCurrentViewer();
    
    /// Resets the current contour
    void resetCurrentContourWidget( );

    /// Completes the current contour
    void completeCurrentContour( );

    /// Handles updating the segmentedImage and views once the contour is accepted
    void acceptCurrentContour( );
    
    /// Updates the overlay of the slice that changed last
    void updateOverlayOnSingle2DViewer( );
    
    /// Pastes the last contour on another slice of the same viewer
    void pasteLastContour();

    /// Remove the contour and corresponding segmented image
    void resetEverything();
    
    /// Save segmented image
    void saveSegmentedImage();
}; 

#endif // SEGMENTOR_H

